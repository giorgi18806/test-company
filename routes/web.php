<?php

use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\EmployeeController;
use App\Http\Controllers\Admin\MainController;
use Illuminate\Support\Facades\Route;



Route::get('/', [MainController::class, 'dashboard'])->name('dashboard');
Route::resource('/departments', DepartmentController::class);
Route::resource('/employees', EmployeeController::class);
