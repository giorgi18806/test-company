@extends('admin.layouts.app')
@section('title', 'Админ-панель')


@section('mainContent')
    <div class="card mb-4">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="card-header">
            <h2>Сводная таблица</h2>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered mt-5">
                    <thead>
                    <tr>
                        <th></th>
                        @foreach($departments as $department)
                            <th>{{ $department->title }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $value)
                        <tr>
                            <td>{{ $value->full_name}}</td>
                            <td>
{{--                                @dd($value->departments->pluck('id'))--}}
                                @if($value->departments->pluck('id')->contains('1'))
                                    <span class="text-success text-center"><i class="fas fa-check"></i></span>
                                @endif
                            </td>
                            <td>
                               @if($value->departments->pluck('id')->contains('2'))
                                    <span class="text-success text-center"><i class="fas fa-check"></i></span>
                                @endif
                            </td>
                            <td>
                               @if($value->departments->pluck('id')->contains('3'))
                                    <span class="text-success text-center"><i class="fas fa-check"></i></span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
