<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/icon.ico') }}">
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="{{ asset('admin-panel/css/main.css') }}" rel="stylesheet"/>
    <link href="{{ asset('admin-panel/css/app.css') }}" rel="stylesheet"/>

    <title>@yield('links')</title>
</head>
<body class="sb-nav-fixed">
@include('admin.includes.topNavbar')
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        @include('admin.includes.leftSidebar')
    </div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                @include('admin.includes.breadcrumb')
                @yield('mainContent')
            </div>
        </main>
       @include('admin.includes.footer')
    </div>
</div>
<script src="{{ asset('admin-panel/js/app.js') }}"></script>

@yield('script')
</body>
</html>
