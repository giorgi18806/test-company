<nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="{{ route('dashboard') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Главная
            </a>
            <a class="nav-link {{ Request::is('/departments') ? 'active' : '' }}" href="{{ route('departments.index') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Отделы
            </a>
            <a class="nav-link {{ Request::is('/employees') ? 'active' : '' }}" href="{{ route('employees.index') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Сотрудники
            </a>
        </div>
    </div>
</nav>
