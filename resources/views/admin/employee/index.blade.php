@extends('admin.layouts.app')
@section('title', 'Отделы')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Сотрудники</li>
@endsection

@section('mainContent')
    <div class="card mb-4">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="card-header text-center">
            <a href="{{ route('employees.create') }}" class="btn btn-success">
                Добавить сотрудника
                <i class="fas fa-plus mr-1"></i>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered mt-5">
                    <thead>
                    <tr>
                        <th>Имя сотрудника</th>
                        <th>Пол</th>
                        <th>Заработная плата</th>
                        <th>Отделы</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $value)
                        <tr>
                            <td>{{ $value->full_name }}</td>
                            <td>{{ $value->gender }}</td>
                            <td>{{ $value->salary }}</td>
{{--                            @dd($value)--}}
                            <td>
                                @foreach($value->departments as $item)
                                    {{ $item->title }} {{ $loop->last ? "" : ',' }}
                                @endforeach
                            </td>
                            <td>
                                <div class="row pl-3">
                                    <a class="btn btn-success btn-sm ml-2"
                                       href="{{ route('employees.edit', $value) }}"><i
                                            class="fas fa-pen-square"></i></a></p>

                                    <form action="{{ route('employees.destroy', $value) }}" method="POST"
                                          id="deleteForm-{{ $value->id }}" style="display: none">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <a class="btn btn-danger btn-sm ml-2" href="" onclick="
                                        if(confirm('Вы действительно желаете удалить этого сотрудника?')){
                                        event.preventDefault();
                                        document.getElementById('deleteForm-{{ $value->id }}').submit();
                                        }else{
                                        event.preventDefault();
                                        }"><i class="fas fa-trash-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
