@extends('admin.layouts.app')
@section('title', 'Сотрудники')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('employees.index') }}">Сотрудники</a></li>
    <li class="breadcrumb-item active">Добавить сотрудника</li>
@endsection

@section('mainContent')
    <section class="content mt-5">
        <div class="card card-primary">
            <form role="form" action="{{ route('employees.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="form-group col-md-4" data-select2-id="38">
                        <label for="first_name">Имя сотрудника</label>
                        <input type="text" class="form-control" id="first_name" name="first_name"
                               placeholder="Введите имя сотрудника" value="{{ old('first_name') }}" autofocus>
                        @error('first_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="middle_name">Отчество сотрудника</label>
                        <input type="text" class="form-control" id="middle_name" name="middle_name"
                               placeholder="Введите отчество сотрудника" value="{{ old('middle_name') }}">
                        @error('middle_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="last_name">Фамилия сотрудника</label>
                        <input type="text" class="form-control" id="last_name" name="last_name"
                               placeholder="Введите фамилию сотрудника" value="{{ old('last_name') }}">
                        @error('last_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="gender">Пол</label>
                        <select class="form-control" name="gender" id="gender">
                            <option value="" selected disabled>Выберите пол</option>
                            <option value="0">Мужской</option>
                            <option value="1">Женский</option>
                        </select>
                        @error('gender') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="salary">Заработная плата</label>
                        <input type="text" class="form-control" id="salary" name="salary"
                               placeholder="Введите заработную плату сотрудника" value="{{ old('salary') }}">
                        @error('salary') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-check col-md-4">
                        <label>Отделы</label><br>
                        @foreach($departments as $department)
                            <input class="form-check-input mx-1" type="checkbox" value="{{ $department->id }}" name="departments[]" id="departments">
                            <label class="form-check-label mx-4" for="departments">{{ $department->title }}</label>
                        @endforeach
                        <br>
                        @error('departments') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    <a href="{{ route('employees.index') }}" class="btn btn-warning">Назад</a>
                </div>
            </form>
        </div>
    </section>
@endsection

