@extends('admin.layouts.app')
@section('title', 'Сотрудники')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('employees.index') }}">Сотрудники</a></li>
    <li class="breadcrumb-item active">Редактировать данные сотрудника</li>
@endsection

@section('mainContent')
    <section class="content mt-5">
        <div class="card card-primary">
            <form role="form" action="{{ route('employees.update', $employee) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="first_name">Имя сотрудника</label>
                        <input type="text" class="form-control" id="first_name" name="first_name"
                               placeholder="Введите имя сотрудника" value="{{ $employee->first_name }}" autofocus>
                        @error('first_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="middle_name">Отчество сотрудника</label>
                        <input type="text" class="form-control" id="middle_name" name="middle_name"
                               placeholder="Введите отчество сотрудника" value="{{ $employee->middle_name }}">
                        @error('middle_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="last_name">Фамилия сотрудника</label>
                        <input type="text" class="form-control" id="last_name" name="last_name"
                               placeholder="Введите фамилию сотрудника" value="{{ $employee->last_name }}">
                        @error('last_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="gender">Пол</label>
                        <select class="form-control" name="gender" id="gender">
{{--                            <option value="" selected disabled>Выберите пол</option>--}}
{{--                            @dd($employee->gender)--}}
                            <option value="0" @if($employee->gender == 'Мужской') selected @endif>Мужской</option>
                            <option value="1" @if($employee->gender == 'Женский') selected @endif>Женский</option>
                        </select>
                        @error('gender') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="salary">Заработная плата</label>
                        <input type="text" class="form-control" id="salary" name="salary"
                               placeholder="Введите заработную плату сотрудника" value="{{ $employee->salary }}">
                        @error('salary') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label>Отделы</label><br>
                        @foreach($departments as $department)
                            <input class="form-check-input mx-1" type="checkbox" value="{{ $department->id }}"
                                   name="departments[]" id="departments" @if($employee->departments->pluck('id')->contains($department->id)) checked @endif>
                            <label class="form-check-label mx-4" for="departments">{{ $department->title }}</label>
                        @endforeach
                        <br>
                        @error('departments') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="{{ route('employees.index') }}" class="btn btn-warning">Назад</a>
                </div>
            </form>
        </div>
    </section>
@endsection

