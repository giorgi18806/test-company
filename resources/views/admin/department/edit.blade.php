@extends('admin.layouts.app')
@section('title', 'Отделы')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('departments.index') }}">Отделы</a></li>
    <li class="breadcrumb-item active">Редактировать отдел</li>
@endsection

@section('mainContent')
    <section class="content mt-5">
        <div class="card card-primary">
            <form role="form" action="{{ route('departments.update', $department) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group mx-3">
                    <label for="title">Название Отдела</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Введите Название Отдела" value="{{ $department->title }}" autofocus>
                    @error('title') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="{{ route('departments.index') }}" class="btn btn-warning">Назад</a>
                </div>
            </form>
        </div>
    </section>
@endsection
