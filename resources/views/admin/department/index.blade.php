@extends('admin.layouts.app')
@section('title', 'Отделы')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Отделы</li>
@endsection

@section('mainContent')
    <div class="card mb-4">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="card-header text-center">
            <a href="{{ route('departments.create') }}" class="btn btn-success">
                Создать Отдел
                <i class="fas fa-plus mr-1"></i>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered mt-5">
                    <thead>
                    <tr>
                        <th>Наименование отдела</th>
                        <th>Количество сотрудников отдела</th>
                        <th>Максимальная заработная плата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($departments as $value)
                        <tr>
                            <td>{{ $value->title }}</td>
                            <td>{{ $value->employees->count() > 0 ? $value->employees->count() : ""}}</td>
                            <td>{{ $value->employees->pluck('salary')->max() }}</td>
                            <td>
                                <div class="row pl-3">
                                    <a class="btn btn-success btn-sm ml-2"
                                       href="{{ route('departments.edit', $value) }}"><i
                                            class="fas fa-pen-square"></i></a></p>

                                    <form action="{{ route('departments.destroy', $value) }}" method="POST"
                                          id="deleteForm-{{ $value->id }}" style="display: none">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <a class="btn btn-danger btn-sm ml-2" href="" onclick="
                                        if(confirm('Вы действительно желаете удалить этот отдел?')){
                                        event.preventDefault();
                                        document.getElementById('deleteForm-{{ $value->id }}').submit();
                                        }else{
                                        event.preventDefault();
                                        }"><i class="fas fa-trash-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
