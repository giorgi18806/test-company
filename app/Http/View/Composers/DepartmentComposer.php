<?php

namespace App\Http\View\Composers;

use App\Models\Admin\Department;
use Illuminate\View\View;

class DepartmentComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('departments', Department::all());
    }
}
