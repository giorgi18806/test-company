<?php

namespace App\Http\View\Composers;

use App\Models\Admin\Employee;
use Illuminate\View\View;

class EmployeeComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('employees', Employee::all());
    }
}
