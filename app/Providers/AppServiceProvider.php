<?php

namespace App\Providers;

use App\Http\View\Composers\DepartmentComposer;
use App\Http\View\Composers\EmployeeComposer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer([
            'admin.pages.dashboard',
            'admin.employee.create',
            'admin.employee.edit',
            'admin.department.index',

        ], DepartmentComposer::class);

        View::composer([
            'admin.pages.dashboard',

        ], EmployeeComposer::class);
    }
}
