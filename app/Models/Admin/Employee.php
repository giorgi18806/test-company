<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getGenderAttribute($attribute)
    {
        return [
            0 => 'Мужской',
            1 => 'Женский',
        ][$attribute];
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->middle_name} {$this->last_name}";
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class);
    }
}
