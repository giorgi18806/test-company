<?php

namespace Database\Seeders;

use App\Models\Admin\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            ['title' => 'Отдел закупок'],
            ['title' => 'Отдел продаж'],
            ['title' => 'PR-отдел']
        ];
//        $departments = [
//            [
//                'title' => 'Отдел закупок',
//                'slug' => 'admin',
//            ],
//
//            [
//                'title' => 'Отдел продаж',
//                'slug' => 'manager',
//            ],
//
//            [
//                'title' => 'Отдел продаж',
//                'slug' => 'manager',
//            ],
//        ];

        foreach ($departments as $item) {
            Department::create($item);
        }
    }
}
